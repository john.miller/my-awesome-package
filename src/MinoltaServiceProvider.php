<?php

namespace Minolta\Minolta;

use Illuminate\Support\ServiceProvider;

class MinoltaServiceProvider extends ServiceProvider
{
    public function register()
    {
      // $this->app->bind('websanova-demo', function() {
      //     return new Demo;
      // });

      $this->mergeConfigFrom(
          __DIR__ . '/config/minolta.php', 'minolta'
      );
    }

    public function boot()
    {
      $this->publishes([
          __DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
      ], 'migrations');

      // $this->publishes([
      //     __DIR__ . '/views' => base_path('resources/views/vendor/websanova-demo')
      // ], 'views');

      $this->publishes([
          __DIR__ . '/config' => config_path('minolta')
      ], 'config');

      require __DIR__ . '/Http/routes.php';

      //$this->loadViewsFrom(__DIR__ . '/views', 'websanova-demo');
    }
}
