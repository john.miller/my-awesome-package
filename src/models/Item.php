<?php

namespace Minolta\Minolta\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'websanova_demo_items';
}
