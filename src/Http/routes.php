<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the Admin.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['prefix' => 'admin', 'as' => 'Admin.', 'namespace' => 'Admin'], function () {
  // dashboard
  Route::get('/', ['as' => 'dashboard', 'uses' => '\Minolta\Minolta\Http\Controllers\AdminController@index']);
  Route::get('/dashboard', ['as' => 'dashboard', 'uses' => '\Minolta\Minolta\Http\Controllers\AdminController@index']);
  Route::get('/model', function () {
    dd(\Minolta\Minolta\Models\Item::get());
  });
});
