<?php

namespace Minolta\Minolta\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class AdminController extends BaseController
{
    /**
     * Display the admin dashboard
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      dd(config('minolta.dashboard'));
    }
}
